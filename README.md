# User Services Microservice

This microservice handles user registration, user login and user profile update. A user can register an account, login and logout as well as updating their details on the update profile page. This microservice will contain API's that will be called by other teams to display and manipulate data.

#Table of Contents
- **[User Services Microservice](#user-services-microservice)**

 	- **[API'S](#markdown-header-apis)**

		* **[Registration](#markdown-header-registration)**

		* **[Login](#markdown-header-login)**

		* **[Delete](#markdown-header-delete)**

		* **[UpdateUser](#markdown-header-updateuser)**

		* **[Users](#markdown-header-users)**

		* **[Forgotpass](#markdown-header-forgotpass)**

		* **[updatePass](#markdown-header-updatepass)**

	- **[Paramaters](#markdown-header-paramaters)**

	- **[Prerequisites](#markdown-header-prerequisites)**

	- **[Installing](#markdown-header-installing)**

	- **[Deployment](#markdown-header-deployment)**

	- **[Database](#markdown-header-database)**

	- **[Built With](#markdown-header-built-with)**

	- **[Authors](#markdown-header-authors)**

	- **[Acknowledgments](#markdown-header-acknowledgments)**



## API'S
##**Registration**

Allows a user to register an account. The inputs this API takes in are: First name, last name, email, password, confirm password, Security answer, Security Answer ID and phone.
The security answer ID is based on the security question that the user selects. The user is stored in the mySQL database which contains all the data inputted.

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/registration

Parameters passed: `fname, lname, email, password, cpassword, SQID, SQanswer, ph_no`

method: `POST`

Expected Output: `{"code": 200, "success":"user registered successfully"}`

![example](https://i.imgur.com/YrOPcwP.png "Example 1")



##**Login**
Allows a registered user to enter their email and password to login to the website. The API will check that the details entered match a user in the mySQL database.

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/login

Parameters passed: `email, password`

method: `POST`

Expected Output: `{"code" : 200,"sucess": "login succesful"}`

![example](https://i.imgur.com/AtPlKNf.png "Example 2")

##**Delete**
An API that is used to delete a user from the database. It takes in a user email as input and deletes the user in the database containing that email

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/delete/

Parameters passed: `email`

method: `DELETE`

Expected Output: `{"code" : 200, "success": "user deleted"}`

![example](https://i.imgur.com/wvfd38t.png "Example 2")

##**UpdateUser**
An API that is used to change user details, it needs a userID as an input which is used to identify the user in the database.  Only first name, last name and phone number can be changed.

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/UpdateUser

Parameters passed: `userid`

method: `PUT`

Expected Output: `{"code": 200, "success": "user updated successfully"}`

![example](https://i.imgur.com/XpZWlGJ.png "Example 2")

##**Users**
An API that will return a single user and their details from the database. It uses email to search for the user.

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/Users/:userid

Parameters passed: `N/A`

method: `GET`

Expected Output: `{"code": 200, "success": "user displayed"}`

![example](https://i.imgur.com/pHyU1k8.png "Example 2")

##**Forgotpass**
This takes in email, and the security question and answer. It then passes this information on to
the updatePass api.

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/Forgotpass

Parameters passed: `email, SQID, SQanswer`

method: `POST`

Expected Output: `code 200, success: Security question and answer correct`

![example](https://i.imgur.com/A1B9Hh8.png "Example 2")

##**updatePass**
Can only be calle from forgotPass.

API URL: http://ec2-107-21-157-76.compute-1.amazonaws.com/updatePass

Parameters passed: `password, userid`

method: `PUT`

Expected Output: `code 200, success: user password updated succesfully`

![example](https://i.imgur.com/L5szBu7.png "Example 2")

###Paramaters

* `userid` A unique id that is automatically generated every time a user is registered.

* `fname` A users first name

* `lname` A users last name

* `email` A users email address

* `password` A users Password

* `cpassword` A confirmation of the initial Password

* `SQID` Security Question ID - This correlates to security answered

* `SQanswer` Security Question Answer - stores the users security question answer


###Prerequisites

This project uses Nodejs. It also needs specific packages, these packages can be installed with the npm command. These are "body-parser,"cryptr", "doco", "express", "mysql.

For running tests, we also used "chai","chai-http" and "mocha"


### Installing
* Make sure git is installed. [Download here](https://git-scm.com/downloads)

* Clone this repository..* Download and install nodeJS[Download here](https://nodejs.org/en/download/)

* Open a bash/cmd and go into the folder where you cloned the repository.

* Type `npm init`. When this runs enter any name, keep pressing enter for all other options.

* Install packages by typing `npm i -D body-parser cryptr docco mysql express`

* To run the server on your localhost, type `node app.js`

* You should see "API server started on: 8081" on the Console

* Open a browser and in the url type `localhost:8081`

* Now you can use the api's



## Deployment
This Miciroservice is run mainly by the bitbucket repository. Each of the team member cloned the repository and then worked on their own local brach. They then pushed their code and merged with the master.

 We used an AWS ec2 instance to run the server. We used Jenkins to automate the deployment process. When someone pushed code to the master, a webhook was set up so that jenkins would automatically build a docker file on the Ec2 instance and run it on the server.

A more detailed look into the CI/CD pipeline and how to set it up for this project can be found [here](https://docs.google.com/document/d/1acsl1arbb2H-csx2T3uLTLL_XmkNkljZkaa7M3c-R_0/edit)

## Database
Our team create a database named “V1project” to manage the users’ information.

Database connection

Hostname: 'mydbinstance.cwbwsphp0mll.eu-west-1.rds.amazonaws.com',
Username : 'V1project',
Password : 'V1project',
Database : 'V1project'.

![example](https://i.imgur.com/4Np5zlp.png "Example 2")

We create two tables in the database, which are “Users” and “SecurityQuestion”.

![example](https://i.imgur.com/jLw4L7b.png "Example 2")

The “Users” table includes userid(primary key), fname, lname, email, password, SQanswer SQID and ph_no.

![example](https://i.imgur.com/oS9YqB0.png "Example 2")
 The other table ,“ SecurityQuestion”, includes 2 columns which are: SQID(primary key) and SQquestion

![example](https://i.imgur.com/EN2Jcu9.png "Example 2")

## Built With

* [NodeJS](https://nodejs.org/en/) - The web framework used
* [npm express](https://www.npmjs.com/package/express) - Managing routes
* [npm body-parser](https://www.npmjs.com/package/body-parser) - To pass data
* [npm cryptr](https://www.npmjs.com/package/cryptr) - Encryption
* [npm mysql](https://www.npmjs.com/package/mysql) - The database method used



## Authors

* **Jignesh Lad**
* **Ayush Jain**
* **Kaiqi Chen**
* **Conor Church**
* **Cillian Greene**


## Acknowledgments

* A special thanks to John Cole & Oisín for all their help and support!
