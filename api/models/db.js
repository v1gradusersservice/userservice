/*
* DB config file
*/

var mysql = require('mysql');

//local mysql db connection
var connection = mysql.createConnection({
    host     : 'mydbinstance.cwbwsphp0mll.eu-west-1.rds.amazonaws.com',
    user     : 'V1project',
    password : 'V1project',
    database : 'V1project'
});
//If it can't connect, an error will be returned
connection.connect(function(err) {
    if (err) throw err;
});
//Exporting the connection object
module.exports = connection;
