// Imports DB module to setup connection with DB
var sql = require('./db.js');


// Cryptr is used to generate the encryption

// Cryptr stores a constant salt for encryption


const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');

//User object constructor, takes in the data from the database
var user = function(user){
 this.fname = user.fname;
 this.lname = user.lname;
 this.email = user.email;
 this.password = user.password;
 this.SQanswer = user.SQanswer;
 this.SQID = user.SQID;
 this.ph_no = user.ph_no;
};


// "register" function to create a new user in the database and assign login priviliges
// The function also takes care of server side validation of the input.

user.register = function createUser(newUser, result) {

    if (newUser.password != newUser.cpassword) {
      console.log(result);
      console.log("Entered password doen't match");
      result({
        "code":204,
        "success":"Entered password doen't match"
      });
    }
    else {
      //SQL query for checking if user with email id exist
      sql.query('SELECT * FROM Users WHERE email = ?',[newUser.email], function (error, results) {
      if (results.length != 0) {
        console.log(result);
        console.log("User already exist");
        result({
          "code":204,
          "success":"User already exist"
        });
      }else{
          // Start of query for registering
          sql.query("INSERT INTO Users (fname,lname, email, password, SQanswer, SQID, ph_no) VALUES (?,?,?,?,?,?,?)",
            [newUser.fname, newUser.lname,newUser.email, cryptr.encrypt(newUser.password), cryptr.encrypt(newUser.SQanswer),
            newUser.SQID, newUser.ph_no], function (err, res) {
              if(err) {
                //Logging the error
                console.error("Error: " + err);

                result({
                  //If SQL query is not succesful, this message will be returned
                  "code":400,
                  "success":"error ocurred"
                });
              }
              else{
                console.log(res);
                console.log("User connected successfully");
                result({
                  //if SQL query is succesful, this message will be returned
                  "code":200,
                  "success":"user registered sucessfully",
                  "user_id":res.insertId
                });
              }
            });   // End of query for registering
          }      // End of if else block
        });
      }
    };


// Function for checking authenticity of the user.

// It checks if the user exists or not and if yes then does the password match
// to what is store in the database

//Start of query for logging in
user.login_user = function authenticate(User , result) {
  //Query selects the user email and finds all the other data associated with that email
  sql.query('SELECT userid, fname, lname, email, ph_no, password FROM Users WHERE email = ?',[User.email], function (error, results) {
    if (error) {
        console.error("Error: " + error);
      result({
        "code":400,
        "failed":"error ocurred"
      })
    }else{
      if(results.length >0){
        console.log(results);
        //This decrypts the hashed password and makes suree that the inputted password matches with the decrypted hashed password
        if(cryptr.decrypt(results[0].password) == User.password){
          console.log(result);
          console.log("login sucessfull");
          result({
            "code":200,
            "success":"login sucessfull",
            "user":results
          });
        }
        else{
          console.log(result);
          console.log("Email and password does not match");
          result({
            //If the email and password entered do not match, this message will be returned
            "code":204,
            "success":"Email and password does not match"
          });
        }
      }
      else{
        console.log(result);
        console.log("Email does not exist");
        result({
          //If  email is not in the database, this message will be returned
          "code":204,
          "success":"Email does not exits"
        });
      }
    }
  });
}


// This function is used for updating user data as displayed in the profile page
// of the website. The update is done based on the userid for the user.

user.update = function updateUser(User, result) {
          // Start of query for updating
          sql.query("UPDATE Users SET fname=?, lname=? WHERE userid =?",
                   [User.fname,User.lname, User.userid], function (err, res) {

            if(err) {
              //Logging the error
              console.error("Error: " + err);
                result({
                  //If SQL query is not succesful, this message will be returned
                  "code":400,
                  "success":"error ocurred"
                });
              }
              else{
                console.log(result);
                console.log("user updated sucessfully");
                result(null,
                {
                  //If SQL query is succesful, this message will be returned
                  "code":200,
                  "success":"user updated sucessfully"
                });
              }
            });
         };


// Function for removing the user from the database. It delete all the details
// given by the user at the time of registering. This is done based on email address


user.remove = function(email,result){
  //SQL query that deletes a user from the database
  sql.query('Delete FROM Users WHERE email = ?',email, function (error, results) {
       if(error){
         //Logging the error
         console.error("Error: " + err);
          result({
           //If SQL query is not succesful, this message will be returned
           "code":400,
           "failed":"error ocurred"
         })
       }else{
         console.log(result);
         console.log("user deleted");
         result({
           //If SQL query is succesful, this message will be returned
           "code":200,
           "success":"user deleted"
         });
       }
     });
   };


// This function fetches all the details of the user to display on the profile page.

// The function executes using a userid and displays all the necessary details apart
// from security details like password, security question and answer.


user.getProfile = function(userid,result){
sql.query('SELECT fname, lname, email, ph_no FROM Users WHERE userid = ?',userid, function (error, results) {
  if(error){
    console.error("Error " + error);
        result({
        "code":400,
        "failed":"error ocurred"
      })
    }else{
      console.log(result);
      console.log("user displayed");
      result({
        "code":200,
        "success":"user displayed",
        "fname":results[0].fname,
        "lname":results[0].lname,
        "email":results[0].email,
        "ph_no":results[0].ph_no
      });
    }
  });
};


// This function in used for forget passwrod mechanism. It changes the password
// for the user if certain criterias are matched.

user.updatePass = function updatePassword(User, result) {
          // Start of query for updating
          console.log(User.password);
          sql.query("UPDATE Users SET password=? WHERE userid =?",
           [cryptr.encrypt(User.password), User.userid], function (err, res) {
             console.log(err);
              if(err) {
                //Logging the error
                console.error("Error: " + err);
                result({
                  //If SQL query is not succesful, this message will be returned
                  "code":400,
                  "success":"error ocurred"
                });
              }
              else{
                console.log(result);
                console.log("user password updated sucessfully");
                result(null,
                {
                  //If SQL query is succesful, this message will be returned
                  "code":200,
                  "success":"user password updated sucessfully"
                });
              }
            }); //End of query for updated
          };





user.verify_user = function securityCheck(User , result) {
  //Query selects the user email and finds all the other data associated with that email
  sql.query('SELECT userid, email, SQID, SQanswer FROM Users WHERE email = ?',[User.email], function (error, results) {
    if (error) {
      console.error("Error" + error);
      result({
        "code":400,
        "failed":"error ocurred"
      })
    }else{
      if(results.length >0){
        console.log(results);
        //Checks Security question and answered entered matches the database question and answer
          if(results[0].SQID == User.SQID && cryptr.decrypt(results[0].SQanswer) == User.SQanswer){
            console.log(result);
            console.log("Security question and answer correct");
            result({
              "code":200,
              "success":"Security question and answer correct",
              userid: results[0].userid
            });
          }
          else{
            console.error("Error "+ error)
            result({
              //If the email and password entered do not match, this message will be returned
              "code":204,
              "success":"Incorrect security question and answer"
            });
          }
      }
      else{
          console.error("Error "+ error)
        result({
          //If  email is not in the database, this message will be returned
          "code":204,
          "success":"Email does not exits"
        });
      }
    }
  });
}




//Exporting the user object to the "userControllers.js" file
module.exports= user;
