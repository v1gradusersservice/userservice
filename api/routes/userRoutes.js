  module.exports = function(app) {

      var user = require('../controllers/userController');

    /*
    * Setting up routes for all the APIs
    */

    app.route('/registration')
    .post(user.register_a_user);

    app.route('/login')
    .post(user.login);

    app.route('/delete/:email')
    .delete(user.delete);

    app.route('/updateUser')
    .put(user.updateUser);

    app.route('/users/:userid')
    .get(user.Get_Profile);

    app.route('/forgotPass')
    .post(user.forgotPass);

    app.route('/updatePass')
    .post(user.updatePassword);


    app.get('/',(req,res)=>{
      res.sendFile(__dirname+'/public/index.html');
    });

    app.get('/a',(req,res)=>{
      res.sendFile(__dirname+'/public/forgetPass1.html');
    });

};
